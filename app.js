const express = require('express')
const app = express()
const port = 3000

/**
 * Add two numbers together
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number} sum of a and b
 */
const add = (a,b) => {
  return a+b;
}






app.get('/', (req, res) => {
  const sum = add(1,2)
  console.log(sum)
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
